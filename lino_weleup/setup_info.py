# -*- coding: UTF-8 -*-
# Copyright 2002-2024 Rumma & Ko Ltd
# License: GNU Affero General Public License v3 (see file COPYING for details)
# test:  $ python setup.py test -s tests.PackagesTests

requires = [
    'lino-welfare', 'pytidylib', 'django-iban', 'metafone', 'cairocffi'
]
requires.append('channels')
requires.append('suds-py3')
# requires.append('suds-jurko')

SETUP_INFO = dict(
    name='lino-weleup',
    version='24.2.0',
    install_requires=requires,
    test_suite='tests',
    tests_require=['pytest'],
    include_package_data=True,
    zip_safe=False,
    description="A Lino Django application for the PCSW of Eupen",
    long_description="""\
**Lino Welfare Eupen** is a
`Lino Welfare <https://welfare.lino-framework.org>`__
application developed and maintained for the PCSW of Eupen (Belgium).

- The central project homepage is http://weleup.lino-framework.org

- For *introductions* and *commercial information*
  see `www.saffre-rumma.net
  <https://www.saffre-rumma.net/welfare/>`__.

- Technical specifications and test suites are in
  `Lino Welfare <https://welfare.lino-framework.org>`__.

- The `German project homepage <https://de.welfare.lino-framework.org>`__
  contains release notes and end-user docs.

- Online demo site at https://weleup1.mylino.net

""",
    author='Rumma & Ko Ltd',
    author_email='info@lino-framework.org',
    url="https://gitlab.com/lino-framework/weleup",
    license_files=['COPYING'],
    classifiers="""\
Programming Language :: Python
Programming Language :: Python :: 3
Development Status :: 5 - Production/Stable
Environment :: Web Environment
Framework :: Django
Intended Audience :: Developers
Intended Audience :: System Administrators
License :: OSI Approved :: GNU Affero General Public License v3
Natural Language :: English
Natural Language :: French
Natural Language :: German
Operating System :: OS Independent
Topic :: Database :: Front-Ends
Topic :: Home Automation
Topic :: Office/Business
Topic :: Sociology :: Genealogy
Topic :: Education""".splitlines())

SETUP_INFO.update(packages=[
    'lino_weleup', 'lino_weleup.lib', 'lino_weleup.lib.pcsw',
    'lino_weleup.lib.pcsw.fixtures'
])

SETUP_INFO.update(include_package_data=True)
