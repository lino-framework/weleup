# -*- coding: UTF-8 -*-
# Copyright 2002-2019 Rumma & Ko Ltd
# License: GNU Affero General Public License v3 (see file COPYING for details)
"""
The main package for :ref:`weleup`.

.. autosummary::
   :toctree:

   lib
   settings



"""

from .setup_info import SETUP_INFO

# doc_trees = ['docs' ] #, 'dedocs']
# srcref_url = 'https://github.com/lino-framework/weleup/blob/master/%s'
doc_trees = []
# weleup has no doctrees, but we want a documentation link for getlino list
intersphinx_urls = dict(docs="https://welfare.lino-framework.org")
