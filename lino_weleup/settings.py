# -*- coding: UTF-8 -*-
# Copyright 2014-2018 rumma & Ko Ltd

from lino_welfare.modlib.welfare.settings import *
from lino_weleup import SETUP_INFO


class Site(Site):
    # use_websockets = True
    verbose_name = "Lino Welfare Eupen"
    description = SETUP_INFO['description']
    version = SETUP_INFO['version']
    url = SETUP_INFO['url']

    help_url = "https://de.welfare.lino-framework.org"
    demo_fixtures = """std std2 few_languages props all_countries
    demo payments demo2 cbss checkdata checksummaries""".split()

    def get_plugin_modifiers(self, **kw):
        kw = super(Site, self).get_plugin_modifiers(**kw)
        kw.update(badges=None)  # remove the badges app
        kw.update(polls=None)
        # kw.update(esf=None)
        # kw.update(projects=None)
        kw.update(immersion=None)
        kw.update(courses=None)
        # kw.update(accounting=None)
        # kw.update(finan=None)
        # kw.update(vatless=None)
        kw.update(active_job_search=None)
        kw.update(pcsw='lino_weleup.lib.pcsw')
        kw.update(cv='lino_welfare.modlib.cv')
        return kw

    # def get_dashboard_items(self, user):
    #     yield self.modules.integ.UsersWithClients
    #     yield self.modules.reception.MyWaitingVisitors
    #     yield self.modules.cal.MyEntries
    #     yield self.modules.cal.MyTasks
    #     yield self.modules.reception.WaitingVisitors
    #     #~ yield self.modules.reception.ReceivedVisitors

    #     if user.is_authenticated:
    #         yield self.models.notify.MyMessages

    def do_site_startup(self):
        ctt = self.models.clients.ClientContactTypes
        ctt.set_detail_layout("""
        id name can_refund is_bailiff
        clients.PartnersByClientContactType
        clients.ClientContactsByType
        """)
        ctt.column_names = "id name can_refund is_bailiff"

        super().do_site_startup()

    def setup_actions(self):
        super().setup_actions()

        from lino.modlib.changes.utils import watch_changes as wc

        wc(self.modules.contacts.Partner)
        wc(self.modules.contacts.Person, master_key='partner_ptr')
        wc(self.modules.contacts.Company, master_key='partner_ptr')
        wc(self.modules.pcsw.Client, master_key='partner_ptr')

        wc(self.modules.coachings.Coaching, master_key='client__partner_ptr')
        wc(self.modules.clients.ClientContact,
           master_key='client__partner_ptr')
        wc(self.modules.jobs.Candidature, master_key='person__partner_ptr')

        # ContractBase is abstract, so it's not under self.modules
        from lino_welfare.modlib.isip.models import ContractBase
        wc(ContractBase, master_key='client__partner_ptr')

        from lino_welfare.modlib.cbss.mixins import CBSSRequest
        wc(CBSSRequest, master_key='person__partner_ptr')

        # self.modules.contacts.Person.disable_create_choice = True
