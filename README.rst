===========================
The ``lino-weleup`` package
===========================



**Lino Welfare Eupen** is a
`Lino Welfare <https://welfare.lino-framework.org>`__
application developed and maintained for the PCSW of Eupen (Belgium).

- The central project homepage is http://weleup.lino-framework.org

- For *introductions* and *commercial information*
  see `www.saffre-rumma.net
  <https://www.saffre-rumma.net/welfare/>`__.

- Technical specifications and test suites are in
  `Lino Welfare <https://welfare.lino-framework.org>`__.

- The `German project homepage <https://de.welfare.lino-framework.org>`__
  contains release notes and end-user docs.

- Online demo site at https://weleup1.mylino.net


